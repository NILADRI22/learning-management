import {Environment} from './env';

export const ProdEnvironment : Environment= {
    db_url: 'mongodb+srv://niladri:niladri@niladridb.yo08p.mongodb.net/user?retryWrites=true&w=majority',
    jwt_secret:'prodSecret'
};
