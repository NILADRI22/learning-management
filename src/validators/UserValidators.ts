import { body, query } from 'express-validator'
import User from '../models/User';

export class UserValidators {
    static signUp() {
        return [body('email', 'Email is Required').isEmail().custom((email,{req})=>{
              return User.findOne({email:email}).then(user=>{
                   if(user)
                   {
                       throw new Error('User Already Exists');
                   }
                   else
                   {
                       return true;
                   }
               })
        }),
            body('password', 'Password is Required').isAlphanumeric()
                .isLength({min: 8, max: 20}).withMessage('Password can be from 8-20 Characters only'),
            body('username', 'Username is Required').isString(),
            body('first_name','Name is Required').isString(),
            body('last_name','Name is Required').isString(),
            body('mobile','Mobile is required').isString().
            isLength({min: 10}).withMessage('Mobile No should be of 10 digits')];
    }
    static verifyUser() {
        return [body('verification_token', 'Verification Token is Required').isNumeric()
            , body('email', 'Email is Required').isEmail()]
    }
    static resendVerificationEmail() {
        return [query('email','Email is Required').isEmail()]
    }
    // user A -> email , password
    // user B -> mobile , password
    static login()
    {
        return [query('email','Email is Required').custom((email,{req})=>{
        
            if(!email && req.query.mobile){
                return true;
            }
                return  User.findOne({email:email}).then(user=>{
                     if(user)
                     {
                         req.user=user;
                         return true;
                     }
                     else{
                         throw new Error('User doesnot Exist');
                     }
                  });
        }),
        query('mobile').custom((mobile,{req})=>{
            if(!mobile && req.query.email){
                return true;
            }
            return  User.findOne({mobile:mobile}).then(user=>{
                if(user)
                {
                    req.user=user;
                    return true;
                }
                else{
                    throw new Error('User doesnot Exist');
                }
             });
        }),
         query('password','Password is required').isAlphanumeric()]
    }
}


